//Exercise15
function draw(n) {

    let i = 1;
    const rows = (n*2) - 1;
    let countNum = 1;

    while (i <= rows) {

        for (let j = 1 ; j <= n ; j++) {

            if (j <= countNum) {

                process.stdout.write('*');

            } else {

                process.stdout.write('-');

            }

        }

        console.log();

        if (i < n) {
            
            countNum++;

        } else {

            countNum--;

        }

        i++;

    }

}

draw(8);