//Exercise10
function draw(n) {

    let i = 1;
    let countNum;

    while (i <= n) {

        countNum = i;

        for (let j = 1 ; j <= n ; j++) {

            process.stdout.write(`${countNum}`);

            countNum += i;
        }

        console.log();

        i++;

    }

}

draw(4);