//Exercise22
function draw(n) {

    let columns = 1;
    const rows = (n*2) - 1;
    let starPos = n;
    let starCount = n;

    while (columns <= n) {
    
        for (let j = 1 ; j <= rows ; j++) {

            if (j > (starPos - starCount) && j < (starPos + starCount)) {

                process.stdout.write('*');

            } else {

                process.stdout.write('-');

            }

        }

        console.log();

        starCount--;

        columns++;

    }

}

draw(10);