//Exercise24
function draw(n) {

    let columns = 1;
    const rows = (n*2) - 1;
    let numPos = n;
    let numCount = 0;
    let number = 1;

    while (columns <= rows) {
    
        for (let j = 1 ; j <= rows ; j++) {

            if (j >= (numPos - numCount) && j <= (numPos + numCount)) {

                process.stdout.write(`${number}`);

                number++;

            } else {

                process.stdout.write('-');

            }

        }

        console.log();

        if (columns < n) {

            numCount++;
        
        } else {

            numCount--;

        }

        columns++;

    }

}

draw(3);