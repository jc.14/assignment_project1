//Exercise13
function draw(n) {

    let i = 1;

    while (i <= n) {

        for (let j = 1; j <= n ; j++) {

            if (j <= i)
            {

                process.stdout.write('*');

            } else {

                process.stdout.write('-');

            }
            
        }

        console.log();

        i++;

    }

}

draw(8);