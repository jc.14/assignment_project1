//Exercise20
function draw(n) {

    let i = 1;
    let rows = (n*2) - 1;
    let dashCount = n;
    let numCount = 1;

    while (i <= rows) {

        for (let j = 1 ; j <= n ; j++) {

            if (j < dashCount) {

                process.stdout.write('-');

            } else {

                process.stdout.write(`${numCount}`);

                numCount++;

            }

        }

        console.log();

        if (i < n) {

            dashCount--;

        } else {

            dashCount++;

        }
    
        i++;

    }

}

draw(3);