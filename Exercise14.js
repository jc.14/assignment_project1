//Exercise14
function draw(n) {

    let i = n;

    while (i >= 1) {

        for (let j = 1; j <= n ; j++) {

            if (j <= i)
            {

                process.stdout.write('*');

            } else {

                process.stdout.write('-');

            }
            
        }

        console.log();

        i--;

    }

}

draw(5);