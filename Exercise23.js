//Exercise23
function draw(n) {

    let columns = 1;
    const rows = (n*2) - 1;
    let starPos = n;
    let starCount = 0;

    while (columns <= rows) {
    
        for (let j = 1 ; j <= rows ; j++) {

            if (j >= (starPos - starCount) && j <= (starPos + starCount)) {

                process.stdout.write('*');

            } else {

                process.stdout.write('-');

            }

        }

        console.log();

        if (columns < n) {

            starCount++;
        
        } else {

            starCount--;

        }

        columns++;

    }

}

draw(6);